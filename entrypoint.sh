#!/bin/bash

export LISTENUP_APP_CONFIG=production

if [ -f /.passwords ]; then
  source /.passwords
fi
if [ -f /.config ]; then
  source /.config
fi

if [ "$SERVICE_URL" != "" ]; then
    python -c "from listenup_common.container import register; register()" &
fi

nginx
