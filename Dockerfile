# Image to build dist package in to prevent needing specific packages on build machine
FROM node:6.15-jessie as builder

RUN mkdir -p /tmp/listenup_ui
COPY listenup_ui/package.json /tmp/listenup_ui/package.json

WORKDIR /tmp/listenup_ui
RUN npm install

COPY listenup_ui/ /tmp/listenup_ui
RUN npm run build

# Using python package plus nginx to do everything... should clean up eventually
FROM python:3.7

RUN apt-get update && apt-get install -y software-properties-common

# Install Nginx.
RUN \
  apt-get update && \
  apt-get install -y nginx && \
  rm -rf /var/lib/apt/lists/* && \
  echo "\ndaemon off;" >> /etc/nginx/nginx.conf && \
  chown -R www-data:www-data /var/lib/nginx

# Define mountable directories.
VOLUME ["/etc/nginx/sites-enabled", "/etc/nginx/certs", "/etc/nginx/conf.d", "/var/log/nginx", "/var/www/html"]

RUN mkdir -p /usr/src/app
RUN mkdir -p /usr/src/app/.wheels

# Define working directory.
WORKDIR /usr/src/app

COPY .wheels /usr/src/app/.wheels

# Using listenup common for registration of service with load balancer
RUN pip install --find-links .wheels --no-cache-dir Listenup-Common==0.15

RUN rm /etc/nginx/sites-enabled/default

COPY nginx.conf /etc/nginx/sites-available/default
COPY entrypoint.sh /usr/src/app/entrypoint.sh
COPY --from=builder /tmp/listenup_ui/dist /usr/share/nginx/html

ARG SERVICE_NAME
ARG SERVICE_VERSION
ENV SERVICE_NAME $SERVICE_NAME
ENV SERVICE_VERSION $SERVICE_VERSION

EXPOSE 80

CMD ["./entrypoint.sh"]
