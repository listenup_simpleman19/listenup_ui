var audio;
var interval = null;
var trackCurrentTime = 0;
var scrubbing = false;

$(document).ready(function(){
  $('#pause').hide();
  audio = $('#audio')[0];

  $('#progress-bar').on("input change", updateRange);
  $('#progress-bar').on("mouseup touchend", setTime);
  $('#progress-bar').on("mousedown touchstart", pauseUpdateing);

  $('#play').click(function() {
      play();
  });

  $('#pause').click(function() {
      pause();
  });

  $('#step-backward').click(function() {
      console.log("Back");
      audio.currentTime = 0;
      updateProgress();
  });

  $('#step-forward').click(function() {
      nextTrack();
  });
});

function nextTrack() {
    console.log("Next Track");
    audio.currentTime = 0;
    updateProgress();
}

function pause() {
    clearInterval(interval);
    interval = null;
    trackCurrentTime = audio.currentTime - 1;
    audio.pause();
    $('#pause').hide();
    $('#play').show();
}

function play() {
    audio.currentTime = trackCurrentTime;
    if (interval === null){
      interval = setInterval(updateProgress, 1000);
    }
    if (audio.paused){
      audio.play();
      $('#play').hide();
      $('#pause').show();
    }
}

function updateProgress() {
  if (!scrubbing) {
    var max = $('#progress-bar').prop('max');
    var progress = Math.floor(audio.currentTime / audio.duration * max);
    $('#progress-bar').val(progress);
    updateRange();
  }
}

function updateRange() {
    percent = $('#progress-bar').val()/$('#progress-bar').prop('max') * 100.0;
    $('#progress-bar').css("background", "-webkit-linear-gradient(left, green 0%, green " + percent + "%, black " + percent + "%)");
    if (scrubbing) {
      time = $('#progress-bar').val();
    } else {
      time = audio.currentTime;
    }
    var currentTime = ("0" + Math.floor((time / 3600) % 24)).slice(-2) + ":";
    currentTime += ("0" + Math.floor((time / 60) % 60)).slice(-2) + ":";
    currentTime += ("0" + Math.floor(time % 60)).slice(-2);
    $('#current-time-p').html(currentTime);
}

function setTime() {
  scrubbing = false;
  play();
  audio.currentTime = $('#progress-bar').val();
  updateRange();
}
function pauseUpdateing() {
  scrubbing = true;
}
