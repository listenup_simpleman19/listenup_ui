import Vue from 'vue'
import Vuex from 'vuex'
import UserService from '@/services/UserService'
import PodcastService from '@/services/PodcastService'
import MarshallerService from '@/services/MarshallerService'
import ListensService from '@/services/ListensService'
import axios from 'axios'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    token: localStorage.getItem("access_token") || null,
    currentUser: JSON.parse(localStorage.getItem("current_user")) || null,
    currentChannel: null,
    currentPodcast: null,
    currentPodcastArt: null,
    currentAudioFiles: null,
    currentAudioState: null,
    lastTimeSent: {
      pod: 0,
      win: 0
    },
    topSubscriptionChannels: [
      {
        id: 1,
        channelName: 'Podcast Channel Name',
        channelArtSrc: 'http://192.168.33.10/api/files/uploads/e69e898173dc4d19b13d703fefa9a0fd',
        podcasts: [
          {
            id: 1,
            title: 'Podcast Name',
            uid: 'fjwiocneafewfa',
            channelId: 1,
            runtime: '1:30',
            files: [
              {
                guid: 'fabeb530eccd4443abf52f3b689fc558',
                mimeType: 'audio/mpeg',
                quality: '128kbps',
                url: 'http://192.168.33.10/api/files/uploads/51c5b9960fa54a359b792fcab6e384e5'
              }
            ]
          },
          {
            id: 2,
            title: 'Podcast Name 2',
            uid: 'fjwiocneafewfwewfa',
            channelId: 1,
            runtime: '1:30',
            files: [
              {
                guid: 'fabeb530eccd4443abf52f3b689fc558',
                mimeType: 'audio/mpeg',
                quality: '128kbps',
                url: 'http://192.168.33.10/api/files/uploads/51c5b9960fa54a359b792fcab6e384e5'
              }
            ]
          },
        ]
      },
      {
        id: 2,
        channelName: 'Podcast Channel Name 2',
        channelArtSrc: 'http://192.168.33.10/api/files/uploads/e69e898173dc4d19b13d703fefa9a0fd',
        podcasts: [
          {
            id: 3,
            title: 'Podcast Name',
            uid: 'fjwiocneafewfa',
            channelId: 1,
            files: [
              {
                guid: 'fabeb530eccd4443abf52f3b689fc558',
                mimeType: 'audio/mpeg',
                quality: '128kbps',
                url: 'http://192.168.33.10/api/files/uploads/51c5b9960fa54a359b792fcab6e384e5'
              }
            ]
          },
          {
            id: 4,
            title: 'Podcast Name 2',
            uid: 'fjwiocneafewfwewfa',
            channelId: 1,
            files: [
              {
                guid: 'fabeb530eccd4443abf52f3b689fc558',
                mimeType: 'audio/mpeg',
                quality: '128kbps',
                url: 'http://192.168.33.10/api/files/uploads/51c5b9960fa54a359b792fcab6e384e5'
              }
            ]
          },
        ]
      },
    ],
    hotPodcastChannels: [
      {
        id: 1,
        channelName: 'Podcast Channel Name',
        channelArtSrc: 'http://192.168.33.10/api/files/uploads/e69e898173dc4d19b13d703fefa9a0fd',
        podcasts: [
          {
            id: 1,
            title: 'Podcast Name',
            uid: 'fjwiocneafewfa',
            channelId: 1,
            files: [
              {
                guid: 'fabeb530eccd4443abf52f3b689fc558',
                mimeType: 'audio/mpeg',
                quality: '128kbps',
                url: 'http://192.168.33.10/api/files/uploads/51c5b9960fa54a359b792fcab6e384e5'
              }
            ]
          },
          {
            id: 2,
            title: 'Podcast Name 2',
            uid: 'fjwiocneafewfwewfa',
            channelId: 1,
            files: [
              {
                guid: 'fabeb530eccd4443abf52f3b689fc558',
                mimeType: 'audio/mpeg',
                quality: '128kbps',
                url: 'http://192.168.33.10/api/files/uploads/51c5b9960fa54a359b792fcab6e384e5'
              }
            ]
          },
        ]
      },
      {
        id: 2,
        channelName: 'Podcast Channel Name 2',
        channelArtSrc: 'http://192.168.33.10/api/files/uploads/e69e898173dc4d19b13d703fefa9a0fd',
        podcasts: [
          {
            id: 3,
            title: 'Podcast Name',
            uid: 'fjwiocneafewfa',
            channelId: 1,
            files: [
              {
                guid: 'fabeb530eccd4443abf52f3b689fc558',
                mimeType: 'audio/mpeg',
                quality: '128kbps',
                url: 'http://192.168.33.10/api/files/uploads/51c5b9960fa54a359b792fcab6e384e5'
              }
            ]
          },
          {
            id: 4,
            title: 'Podcast Name 2',
            uid: 'fjwiocneafewfwewfa',
            channelId: 1,
            files: [
              {
                guid: 'fabeb530eccd4443abf52f3b689fc558',
                mimeType: 'audio/mpeg',
                quality: '128kbps',
                url: 'http://192.168.33.10/api/files/uploads/51c5b9960fa54a359b792fcab6e384e5'
              }
            ]
          },
        ]
      },
    ]
  },
  getters: {
    getToken: state => {
      return state.token
    },
    loggedIn: state => {
      return state.token !== null && state.token.trim() !== '';
    },
    getCurrentUser: state => {
      return state.currentUser;
    },
    getCurrentChannel: state => {
      return state.currentChannel
    },
    getCurrentPodcast: state => {
      return state.currentPodcast
    },
    getTopSubscriptionChannels: state => {
      return state.topSubscriptionChannels
    },
    getHotPodcastChannels: state => {
      return state.hotPodcastChannels
    },
    getPodcast: state => {
      return state.currentPodcast
    },
    getAudioFiles: state => {
      return state.currentAudioFiles
    },
    getAudioState: state => {
      return state.currentAudioState
    }
  },
  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token;
    },
    CLEAR_TOKEN: (state) => {
      state.token = null;
    },
    SET_CURRENT_USER: (state, user) => {
      state.currentUser = user;
    },
    CLEAR_CURRENT_USER: (state) => {
      state.currentUser = null;
    },
    SET_CURRENT_PODCAST: (state, podcast) => {
      state.lastTimeSent = {
      pod: 0,
      win: 0
    };
      state.currentPodcast = podcast;
    },
    SET_CURRENT_AUDIO_FILES: (state, files) => {
      state.currentAudioFiles = files;
    },
    CLEAR_CURRENT_AUDIO_FILES: (state) => {
      state.currentAudioFiles = null;
    },
    UPDATE_AUDIO_STATE: (state, audio) => {
      state.currentAudioState = audio;
    },
    SET_LAST_TIME_SENT: (state, sent) => {
      state.lastTimeSent = sent;
    }
  },
  actions: {
    loginUser(context, credentials) {
      console.log("User Login");
      return new Promise((resolve, reject) => UserService.login(credentials.username, credentials.password)
        .then(response => {
          const token = response.data.token;
          const user = response.data.user;
          localStorage.setItem("access_token", token);
          context.commit("SET_TOKEN", token);
          localStorage.setItem("current_user", JSON.stringify(user));
          context.commit("SET_CURRENT_USER", user);
          resolve();
        })
        .catch(error => {
          console.log(error);
          localStorage.removeItem("access_token");
          localStorage.removeItem("current_user");
          context.commit("CLEAR_TOKEN");
          context.commit("CLEAR_CURRENT_USER", null);
          reject();
        }));
    },
    logoutUser(context) {
      console.log("User logging out");
      localStorage.removeItem("access_token");
      localStorage.removeItem("current_user");
      context.commit("CLEAR_TOKEN");
      context.commit("CLEAR_CURRENT_USER", null);
    },
    validateToken(context) {
      console.log("Validateing token");
      UserService.validateToken()
        .then((response) => {
          console.log("Validated user token");
          let token = response.data.token;
          let user = response.data.user;
          localStorage.setItem("access_token", token);
          context.commit("SET_TOKEN", token);
          localStorage.setItem("current_user", JSON.stringify(user));
          context.commit("SET_CURRENT_USER", user);
        })
        .catch((e) => {
          console.log(e);
          localStorage.removeItem("access_token");
          localStorage.removeItem("current_user");
          context.commit("CLEAR_TOKEN");
          context.commit("CLEAR_CURRENT_USER", null);
        })
    },
    loadPodcast(context, guid) {
      console.log("Loading podcast");
      console.log(guid);
      return new Promise((resolve, reject) => {
          PodcastService.getPodcastForGuid(guid)
            .then(response => {
              let podcast = response.data;
              console.log(podcast);
              context.commit("SET_CURRENT_PODCAST", podcast);
              resolve(podcast);
            }).catch( (err) => {
              console.log("Error getting podcast");
              reject(err);
          });
        });
      },
    loadFilesAndPodcast(context, guid) {
      console.log(guid);
      if (context.state.currentPodcast == null || guid !== context.state.currentPodcast.guid) {
        context.dispatch("loadPodcast", guid).then((podcast) => {
          console.log("Getting files for podcast");
          console.log(podcast);
          context.commit("CLEAR_CURRENT_AUDIO_FILES");
          let calls = [];
          podcast['files'].forEach((file) => {
            calls.push(MarshallerService.getUrlsForFile(file.file_guid));
          });
          console.log(calls);
          let files = [];
          axios.all(calls).then((respArr) => {
            respArr.forEach((resp) => {
              files.push(resp.data)
            });
            console.log(files);
            context.commit("SET_CURRENT_AUDIO_FILES", files);
            context.dispatch("loadCurrentLocation", guid);
          });
        })
      }
    },
    loadCurrentLocation(context, podacst_guid) {
      console.log("Loading current location for podacast");
      ListensService.getCurrentTimeForPodcast(podacst_guid).then(r => {
        console.log(r.data);
        eventBus.$emit("audioSetTime", r.data.time_seconds);
      }).catch(err => {
        console.log(err);
      })
    },
    recordCurrentTimeForUser(context) {
      // Added the window performance thing for scrubbing, try not to blow up NIC or service when scrubbing...
      let lastSent = context.state.lastTimeSent;
      if (context.state.currentAudioState !== null
        && Math.abs(context.state.currentAudioState.currentTimeSeconds - lastSent.pod) > 8
        && Math.abs(window.performance.now() - lastSent.win) > 4000) {
        console.log("Recording time for user");
        ListensService.recordTime(
          context.state.currentPodcast.guid,
          Math.round(context.state.currentAudioState.currentTimeSeconds)
        ).then(r => {
          context.commit("SET_LAST_TIME_SENT", {
            pod: context.state.currentAudioState.currentTimeSeconds,
            win: window.performance.now()
          });
          console.log("Successfully recorded time for user");
        }).catch(err => {
          console.log(err);
        })
      }
    }
  }
})
