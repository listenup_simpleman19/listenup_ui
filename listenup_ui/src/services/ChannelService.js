import Api from '@/services/Api'

export default {
  getAllChannels () {
    return Api().get('channel/all');
  },
  getChannel(guid) {
    return Api().get('channel/' + guid);
  },
  createChannel(title, description, category, language, subtitle) {
    return Api().post('channel/add', {
      title: title,
      description: description,
      category: category,
      language: language,
      itunes_subtitle: subtitle,
    });
  },
  updateChannel(guid, title, description, itunes_subtitle, category) {
    return Api().put('channel/' + guid, {
      title: title,
      description: description,
      itunes_subtitle: itunes_subtitle,
      category: category,
    })
  },
  getCategories() {
    return Api().get('channel/categories');
  },
  getChannelsForUser(guid) {
    return Api().get('channel/user/' + guid);
  },
  getPodcastsForChannel(guid) {
    return Api().get('channel/' + guid + '/podcasts');
  },
  addPodcast(channelGuid, podcastGuid) {
    return Api().post('channel/' + channelGuid + '/podcast', { podcast_guid: podcastGuid });
  }
}
