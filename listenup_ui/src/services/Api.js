import axios from 'axios'
import store from '@/store/store'

function retryFailedRequest (err) {
  if ((err.status === 500 || err.status === 501 || err.status === 502) && err.config && !err.config.__isRetryRequest) {
    console.log("Retrying got a " + err.status);
    err.config.__isRetryRequest = true;
    return axios(err.config);
  }
  throw err;
}
axios.interceptors.response.use(undefined, retryFailedRequest);

export default () => {
    return axios.create({
        baseURL: location.origin + '/api/',
        withCredentials: false,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${store.getters.getToken}`
        }
    })
}
