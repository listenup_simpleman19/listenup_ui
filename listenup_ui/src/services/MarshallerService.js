import Api from '@/services/Api'

export default {
  getUrlsForFile(guid) {
    return Api().get('marshaller/file/' + guid);
  },
  getUrlForImageGuid(guid) {
    return new Promise((resolve, reject) => this.getUrlsForFile(guid).then((r) => {
          resolve(r.data.primary);
    }).catch(error => {
      reject(error);
    }));
  }
}
