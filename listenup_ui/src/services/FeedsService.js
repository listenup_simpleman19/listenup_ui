import Api from '@/services/Api'

export default {
  getMyFeed () {
    return Api().get('feeds/');
  },
  getHotFeed(guid) {
    return Api().get('feeds/hot');
  },
  getTop10Feed(guid) {
    return Api().get('feeds/top/10');
  },
}
