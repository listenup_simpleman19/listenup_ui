import Api from '@/services/Api'

export default {
  getPodcastForGuid(guid) {
    return Api().get('podcast/' + guid);
  },
  createPodcast(title, description, author, language) {
    return Api().post('podcast/add', {
      title: title,
      description: description,
      author: author,
      language: language,
    })
  },
  uploadFile(guid, file) {
    let formData = new FormData();
    formData.append('file', file);

    return Api().put( 'podcast/' + guid + '/upload', formData, {headers: { 'Content-Type': 'multipart/form-data' }});
  }
}
