import Api from '@/services/Api'

export default {
  getAllRssFeeds () {
    return Api().get('rss/all');
  },
  getItemsForFeed(guid) {
    return Api().get('rss/' + guid + '/items');
  },
  getItemsForFeedChannelStyle(guid) {
    return Api().get('rss/' + guid + '/items?channel_style=true');
  },
  getRssChannel(guid) {
    return Api().get('rss/' + guid)
  },
  addRssFeed(url) {
    return Api().post('rss/add', { feed_url: url })
  }
}
