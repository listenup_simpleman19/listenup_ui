import Api from '@/services/Api'

export default {
  recordTime (podcast_guid, currentTime) {
    return Api().post('listens/record', {
      podcast_guid: podcast_guid,
      time_seconds: currentTime
    });
  },
  getCurrentTimeForPodcast(podcast_guid) {
    return Api().get('listens/location/' + podcast_guid);
  }
}
