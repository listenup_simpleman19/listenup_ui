import Api from '@/services/Api'

export default {
  login (username, password) {
    return Api().post('users/login', {}, {
      auth: {
          username: username,
          password: password
      }
    });
  },
  validateToken () {
    return Api().post('users/me');
  },
  getUserData () {
    return Api().get('users/me');
  },
  registerUser(username, display_name, email, password, confirm_password, accept_tos) {
    let signUpBody = new FormData();

    signUpBody.set('username', username);
    signUpBody.set('display_name', display_name);
    signUpBody.set('email', email);
    signUpBody.set('password', password);
    signUpBody.set('confirm', confirm_password);
    signUpBody.set('accept_tos', accept_tos);

    // username: username,
    // display_name: display_name,
    // email: email,
    // password: password,
    // confirm: confirm_password,
    // accept_tos: accept_tos

    return Api().post('users/signup', signUpBody, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    });
  },
  updateUserInfo(username, display_name, email) {
    return Api().put('users/me', {
      username: username,
      display_name: display_name,
      email: email,
    });
  },
  getUser(guid) {
    return Api().get('users/' + guid);
  }
}
