import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Listen from '@/components/Listen'
import UserFeedRss from '@/components/feeds/UserFeedRss'
import BrowseRss from '@/components/feeds/BrowseRss'
import AddRssFeed from '@/components/feeds/AddRssFeed'
import RssChannel from '@/components/channels/RssChannel'
import ViewChannel from '@/components/channels/ViewChannel'
import Logout from '@/components/auth/Logout'
import Login from '@/components/auth/Login'
import SignUp from '@/components/auth/SignUp'

Vue.use(Router);
import store from '@/store/store'
import CreateChannel from "../components/channels/CreateChannel";
import RequireAuthentication from "../components/auth/RequireAuthentication";
import MyAccount from "../components/account/MyAccount";
import CreatePodcast from "../components/podcasts/CreatePodcast";
import EditPodcast from "../components/podcasts/EditPodcast";

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/authRequired',
      name: 'RequireAuthentication',
      component: RequireAuthentication
    },
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/account',
      name: 'MyAccount',
      component: MyAccount
    },
    {
      path: '/listen/:guid',
      name: 'Listen',
      component: Listen
    },
    {
      path: '/channel/:guid',
      name: 'Channel',
      component: ViewChannel
    },
    {
      path: '/createchannel',
      name: 'CreateChannel',
      component: CreateChannel
    },
    {
      path: '/createpodcast/:guid',
      name: 'CreatePodcast',
      component: CreatePodcast
    },
    {
      path: '/editpodcast/:guid',
      name: 'EditPodcast',
      component: EditPodcast
    },
    {
      path: '/signup',
      name: 'SignUp',
      component: SignUp
    },
    {
      path: '/rssfeed',
      name: 'UserFeedRss',
      component: UserFeedRss
    },
    {
      path: '/rssbrowse',
      name: 'BrowseRss',
      component: BrowseRss
    },
    {
      path: '/rssadd',
      name: 'AddRssFeed',
      component: AddRssFeed
    },
    {
      path: '/rsschannel/:guid',
      name: 'RssChannel',
      component: RssChannel
    },
    {
      path: '/logout',
      name: 'Logout',
      component: Logout
    },
  ]
});

router.beforeEach((to, from, next) => {
  if (!store.getters.loggedIn && to.name !== 'RequireAuthentication') {
    next('/authRequired');
  } else {
    next();
  }
});

export default router;
